# Library of Ruina Lore Run Rules

A "lore run" is a simulation of a first playthrough, one where the runner slowly unravels all of the mysteries of the game’s story and setting. 

In this `Library of Ruina` lore run, we take the role of the clueless Roland and the naive Angela.

## Goals

**Primary Goals**

* Complete all Receptions, including General Receptions
* Get all Key Pages and Pages

**Optional Goal**

* Complete all Achievements

## Rules

* You must watch all of the cutscenes at least once without skipping.
* You must read all of the character dialogue during receptions. Taking voice acting seriously is optional.
* You must read as much lore-related text as possible. This includes:
  * everything in the Credenza
  * card art
  * E.G.O. details
  * Ordeal descriptions (if possible)
  * Abnormality and boss battle background text
* You must unlock all pages from the current chapter before moving on to the next chapter.
* You must try your best to roleplay the librarian’s lack of knowledge of the future. Information related to events later in the game must not be discussed. (See Additional Rules below for `Lobotomy Corporation` spoilers) Remember you can still back out of a reception without losing books after taking a peek at the guests and their decks.
* You can still engage with your chat, but you should consider timing out or even banning chat members giving out too many spoilers. Similarly, you should also consider removing chat from the overlay to remove the chance of spoilers showing up on the stream and VODs.
* Unresearched Abnormalities must be randomly picked through an external randomizer (eg. Random.org, roll dice, etc). You can choose an Abnormality if all presented are already researched.
* Proper pronunciation of names and terms (eg. aengela vs angela, sephirah names, Distorted Yan as gibberish) is optional but recommended.
* Appearance mods are ok, but avoid using gameplay mods. If you plan to use them, at least have a lore-friendly reason/excuse.

## Additional Rules

You can add any of the following to customize your run:

* Whether treat this as a standalone run (i.e. Roland-leaning run, `Lobotomy Corporation` spoilers must not be discussed until they are brought up) or as a follow-up to a `LobCorp` lore run (i.e. spoilers from the previous game are allowed).
* Choose the order to tackle receptions:
  * Complete them according to their early access release dates. This approach is probably more lore-friendly, as it’s likely that the events of the game happened in this order (except for the Keter Realization and General Receptions).
  * Complete them efficiently, ie. from easiest to hardest, gaining pages that will give you a higher chance of consecutive successes.
  * Complete them according to your whim, eg. complete connected receptions to receive pages that synergize with each other.