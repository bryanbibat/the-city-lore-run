# Lobotomy Corporation Lore Run Rules

A "lore run" is a simulation of a first playthrough, one where the runner slowly unravels all of the mysteries of the game’s story and setting. 

In this `Lobotomy Corporation` lore run, we take the role of the new site manager.

## Goals

**Primary Goals**

* Complete all Missions
* Research all Abnormalities
* Get all Endings

**Optional Goal**

* Defeat all Ordeal types

## Rules

* You must read all of the conversations in the story portion of the game. Taking voice acting seriously is optional.
* You must read as much lore-related text as possible. This includes:
  * flavor text popping up above the containment units during initial work actions
  * staff mumblings and comments from Angela and the Sefirah
  * E.G.O. details
  * Department info once they’re opened
  * Mission details
  * Ordeal descriptions (if possible)
  * Suppression background text
* Unresearched Abnormalities must be randomly picked through an external randomizer (eg. Random.org, roll dice, etc). You can choose an Abnormality if all presented are already researched.
* Initial work actions (ie. *Instinct* vs *Insight* vs *Attachment* vs *Repression*) on unresearched Abnormalities also must be chosen randomly, simulating the manager’s lack of information. Actions can be repeated if the previous result was Good. Actions can be chosen freely once the *Work Favor* is fully researched.
* Information must be unlocked in the following order: *Managerial Works*, *Work Favor*, *Basic Information*, and *Escape Information*.
* After every unlock, you must also read the newly unlocked Story for the Abnormality.
* Retries are allowed, but returning to memory imprint (or first day) must be avoided except for extremely run-ending Abnormality combinations.
* Other exceptions to avoiding returning to the first day would be after reaching a bad ending (lore-wise, this point you should go back to 1st day) and preparing for the true ending after getting Ending C (ie. completing Encyclopedia and getting the right Abnormalities).
* You must try your best to roleplay the manager’s lack of knowledge. Information related to events later in the game must not be discussed, and anything related to `Library of Ruina` must ***NOT*** be mentioned.
* You can still engage with your chat, but you should consider timing out or even banning chat members giving out too many spoilers. Similarly, you should also consider removing chat from the overlay to remove the chance of spoilers showing up on the stream and VODs.
* Going through the tutorial instead of starting the first day at the beginning of the run is optional but recommended.
* Apart from “Waw” ([vav](https://forvo.com/search/%D7%95%D6%B8%D7%95/)), proper pronunciation of names and terms (eg. aengela vs angela, sephirah names) is optional but recommended.
* Appearance mods are ok, but avoid using gameplay mods. If you plan to use them, at least have a lore-friendly reason/excuse.

## Additional Rules

You can add any of the following to customize your run:

* Whether or not to skip Tumblebug / crowdfunding backer and staff Abnormalities (those starting with "D-").
* Department opening order e.g. according to how department scrolling works in-game, or randomly.